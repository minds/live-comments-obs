<h1 align="center">
  Live comments for OBS
  <br>
</h1>

![Live comments](/demo/demo.jpeg "Live comments & votes")

# Usage

There are two ways to use this plugin.

## Using the demo web site:

There is a live demo on https://live-comments-obs.vercel.app

To add the live comments to OBS:

- Add a Browser source in OBS
- Set the URL to https://live-comments-obs.vercel.app/?link=POST_LINK

For instance `https://live-comments-obs.vercel.app/?link=https://www.minds.com/newsfeed/1531655497172652050`

![OBS](/demo/obs.jpeg "OBS Settings")

## Running it locally

For simplicity, we are using Node & [serve](https://www.npmjs.com/package/serve) on this example, but any web server will work.

Clone the repository

```bash
git clone git@gitlab.com:minds/live-comments-obs.git
```

Move to the folder and start the server

```bash
cd live-comments-obs
npx serve
```
Use the localhost on OBS `http://localhost:3000/?link=POST_LINK`

For instance:
`http://localhost:3000/?link=https://www.minds.com/newsfeed/1531655497172652050`

## Customization

You can disable the up votes feedback by adding the `disableLikes=1` parameter to the URL

For instance `https://live-comments-obs.vercel.app/?link=https://www.minds.com/newsfeed/1531655497172652050&disableLikes=1`

# Development

This project is using all the dependencies via CDN so it doesn't need any building process.
You can use `npx live-server` it will reload your website whenever you change any file.
